<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sign Up | SanberBook</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="POST">
		@csrf

		<p>First name:</p>		
		<input type="text" name="firstname">
		<p>Last name:</p>
		<input type="text" name="lastname">
		
		<p>Gender:</p>
		<input type="radio" id="male" name="gender" value="male">
		<label for="male">Male</label><br>
		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label><br>
		<input type="radio" id="other1" name="gender" value="other">
		<label for="other1">Other</label>

		<p>Nationality:</p>
		<select name="nationality">
			<option value="indonesian">Indonesian</option>
			<option value="singaporean">Singaporean</option>
			<option value="malaysian">Malaysian</option>
			<option value="indian">Indian</option>
		</select>

		<p>Languange Speakers:</p>
		<input type="checkbox" id="indonesia" name="lang" value="indonesia">
		<label for="indonesia">Indonesia</label><br>
		<input type="checkbox" id="english" name="lang" value="english">
		<label for="english">English</label><br>
		<input type="checkbox" id="other2" name="lang" value="other">
		<label for="other2">Other</label>

		<p>Bio:</p>
		<textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>	
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>